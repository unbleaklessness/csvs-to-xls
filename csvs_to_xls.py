import sys, csv

def is_valid_csv_row(row):
    if row == []:
        return False
    for value in row:
        if value == '':
            continue
        try:
            float(value)
        except Exception:
            return False
    return True

def main():
    try:
        import xlsxwriter
    except ImportError:
        exit("This script requires XlsxWriter python package!")

    if len(sys.argv) < 3:
        print('Expected minimum 2 arguments!')

    xls_name = sys.argv[-1]
    csv_names = sys.argv[1 : len(sys.argv) - 1]

    workbook = xlsxwriter.Workbook(xls_name)
    worksheet = workbook.add_worksheet()

    row_add_next = 0
    row_add = 0

    for csv_name in csv_names:
        row_subtract = 0
        with open(csv_name) as csv_file:
            reader = csv.reader(csv_file, delimiter = ',')
            for row_index, row in enumerate(reader):
                if not is_valid_csv_row(row):
                    row_subtract += 1
                    continue
                row_add_next += 1
                for column_index, value in enumerate(row):
                    worksheet.write(row_index - row_subtract + row_add, column_index, value)
        row_add += row_add_next

    workbook.close()

if __name__ == '__main__':
    main()